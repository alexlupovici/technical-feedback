describe('area building process', function() {
	var json;
	describe('load data', function() {
		it('valid json url - should load without error', function(done) {
			data.load('../data/web.json', function(result){
				if((json = result))
					done();
			});
		});
		
		it('invalid json url - should not load', function(done) {
			data.load('../data/web2.json', function(result){
				if(!result)
					done();
			});
		});
	});
	
	var areas;
	var areaLegends;
	
	describe('build dom areas', function() {
		it('section title exists', function(done) {
			builder.build(document.getElementById('areas'), function(result){
				console.log("areas ready");
				
				areas = $('#areas');
				areaLegends = areas.find('fieldset').find('legend');
				var title = $('#areas h3');
				console.log("------- ", areaLegends.eq(0).width(), areaLegends.eq(0).position(), '\n', areaLegends.html(), ' ---------- \n');
				if(title.length === 1) {
					done();
				}
			}, '../data/web.json');
		});
		
		
		it('total number of areas in json is equal with created dom elements', function()	{
				assert.equal(areas.find('fieldset').length, json.areas.length)
			})
		
		it('all areas have correct title', function(){
			
			assert.equal(areas.find('fieldset').length, json.areas.length);
			
			$.each(json.areas, function(index, area) {
				console.log("area name =", areaLegends.eq(index).text(), area.name, index);
				assert.equal(areaLegends.eq(index).text(), area.name);
			});
			
			console.log("all areas exists test");
			var areas2 = areas;
			var x = areas.find('fieldset');
			var firstArea = areas.find('fieldset').filter(':first');
			//firstArea.remove();
			var firstItem = firstArea.find('p').filter(':first');
			firstItem.remove();
			//console.log("equality between jquery elements", x.html(), areas2.html());
			
			//x.appendTo('body');
		});
});
});