var assert = chai.assert;
describe('Iterator', function() {
	var arr;
	var iterator;
	it('receive null array', function() {
		iterator = data.iteratorFor(null);
		assert.equal(iterator.currentItem(), null);
		assert.equal(iterator.hasNext(), false);
		assert.equal(iterator.next().currentItem(), null);
	});
	
	it('receive empty array', function() {
		arr = [];
		iterator = data.iteratorFor(arr);
		
		assert.equal(iterator.hasNext(), false);
	});
	
	it('receive valid array', function() {
		arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		
		iterator = data.iteratorFor(arr);
		assert.equal(iterator.currentItem(), 1);
		assert.equal(iterator.next().next().next().currentItem(), 4);
		assert.equal(iterator.next().next().next().next().next().next().hasNext(), false);
		assert.equal(iterator.next().currentItem(), null);
		
		iterator.reset();
		assert.equal(iterator.currentItem(), 1);
		
	});
});