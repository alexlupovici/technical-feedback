function JsonLoader() {
};

JsonLoader.prototype.load = function(url, callback) {
	var request = new XMLHttpRequest();
	request.overrideMimeType("application/json");
	
	console.log('load json from file ' + url + " this = " + this);
	
	request.onreadystatechange = function() {
		console.log('request ready state change [' + request.readyState + ', ' + request.status + ']');
		
		if(request.readyState == 4 && request.status == 200) {
			callback(request.responseText)
		}
	};
	
	request.open('GET', url, true);
	request.send();
};