/** This function handles the loading of data. It also contains an util class for iteration through data items.
 *  It returns an API with methods for:
		- loading data from an url
		- generating an iterator for a specific list of items
 */

define([], function() {
	function JsonData() {
		/**
		 *	Initial state
		 */
		this.data = null;
		this.state = JsonData.States.IDLE;
	}

	JsonData.prototype.load = function(source, onDone) {
		if (!source) {
			console.log('[[Warn]]: JsonData.load(' + source + ', ' + onDone + '): Invalid source parameter.');
			return;
		}
		
		if(typeof(source) === "string") {
			//use DataLoader to load data
			this.state = JsonData.States.LOADING;
			var _this = this;
			DataLoader.load(source, 'application/json', function(response) {
															if(response.status === DataLoader.RESULT.SUCCESS) {
																try {
																	_this.data = JSON.parse(response.content);
																	_this.state = JsonData.States.READY;

																}
																catch (error) {
																	_this.state = JsonData.States.IDLE;
																	console.log('[[Error]]: JsonData - JSON malformed!');
																}
																
																if(typeof(onDone) === "function") {
																	onDone(_this.data);
																}
															}
															else {
																//throw new Error(response.content);
																//reponse.status === fail. onDone() is called without parameter, meaning that no data was loaded.
																onDone();
															}
														}
							);
		}
		else {
			data = source;
			this.areaIterator = new Iterator(this.data);
			this.state = JsonData.States.READY;
		}
	};

	JsonData.States = {
		IDLE: 0,
		LOADING: 1,
		READY: 2,
	};

	DataLoader = {
		load : function(url, mimeType, callback) {
			var request = new XMLHttpRequest();
			request.overrideMimeType(mimeType);
			
			console.log('load json from file ' + url + " this = " + this);
			
			request.onreadystatechange = function() {
				console.log('request ready state change [' + request.readyState + ', ' + request.status + ',' + request.responseText + ']');
				
				if(request.readyState == 4) {
					if(request.status == 200) {
						callback({ status: 'success', content: request.responseText});
					}
					else {
						callback({ status: 'fail', content: request.statusText})
					}
				};
			};
			
			request.open('GET', url, true);
			request.send();
		},
		RESULT : {
			SUCCESS: 'success',
			FAIL: 'fail'
		}
	};


	/**
	 * This class has the purpose of iteration.
	 * params: data - the collection (array for iteration)
	 */
	function Iterator(data) {
		console.log("New iterator ", Array.isArray(data));
		this.data = data || [];
		this.position = this.data.length > 0 ? 0 : -1;
	}

	Iterator.prototype.getItemAtIndex = function(index) {
		if(isNaN(index) || index < 0 || index >= this.data.length) {
			console.log('[[Warn]]: Iterator.getItemAtIndex(' + index + '): Incorrect type of index or index out of bounds!');
			return null;
		}
		
		return this.data[index];
	};

	Iterator.prototype.currentItem = function() {
		if(this.position < 0 || this.position > this.data.length) {
			console.log('[[Warn]]: Iterator.currentItem(): Item with index ' + this.position + ' is not existing!');
			return null;
		}
		return this.data[this.position];
	};

	Iterator.prototype.hasNext = function() {
		return this.position >= -1 && this.position < this.data.length - 1;
	};

	Iterator.prototype.next = function() {
		if(++this.position >= this.data.length) {
			console.log('[[Warn]]: Iterator.next(): End of data iteration.');
			//return null;
		}
		return this;
	};

	Iterator.prototype.reset = function() {
		this.position = 0;
		return this;
	};
	
	return {
		load: function(source, onDone){
			var jsonData = new JsonData();
			jsonData.load(source, onDone);
		},
		
		iteratorFor: function(list) {
			return new Iterator(list);
		}
	};
});

