define(["sources/jsonData"], function(data) {
	//var builder = new AreaBuilder(root, url, callback);
	
	/** A.
	 * This class will manage the dynamic creation of job areas.
	 * params: - root: is the parent DOM element that will live in
	 *		   - url: is the url for loading json data.
			   - callback: options param that will be called when AreaBuilder is ready (data loaded and view created)
	 */
	function AreaBuilder(root, onDone, url) {
		this.jsonUrl = url;// = './data/specialization.json';//'http://mysafeinfo.com/api/data?list=englishmonarchs&format=json';
		
		/**
		* Default values for mandatory fields that are missing from json data
		*/
		AreaBuilder.Defaults = {
			SPECIALIZATION: "web",
			ARIA_NAME: "Arie / Calificativ",
		};

		/** 
		* String consts for identifying item types 
		*/
		AreaBuilder.List = {
			SPECIALIZATIONS : 'specializations',
			CATEGORIES : 'categories',
			AREAS : 'areas',
			ITEMS : 'items'
		};
		
		//view will keep a reference for parent element
		this.view = root;
		this.currentArea;
		this.currentCategory;
		this.specialization;
		
		var _this = this;
		
		if(!this.jsonUrl) {
			var baseDataPath = './data/';
			//var specialization = AreaBuilder.Defaults.SPECIALIZATION;
			var dataFormat = '.json';
			var queryString = window.location.search.substring(1);
			var vars = queryString.split('&');
			
			this.specialization = AreaBuilder.Defaults.SPECIALIZATION;
			//try to find the 's' parameter which contains the specialization
			for(var i = 0; i < vars.length; i++) {
				var parts = vars[0].split('=');
				if(parts[0] === 's') {
					this.specialization = parts[1];
					break;
				}
			}
			
			//construct url to load data
			this.jsonUrl = baseDataPath + this.specialization + dataFormat;
		}
		
		var titleSpec = document.getElementById('title-spec');
		if(titleSpec) {
			titleSpec.innerHTML = this.specialization;
		}
		
		console.log("jsonUrl = " + this.jsonUrl);
		
		data.load(this.jsonUrl, function(jsonData) {
						_this.view.insertBefore(_this.process(jsonData, 'specializations'), document.getElementById('general-info').nextSibling);
						onDone();
					});
	}

	/** This method will construct a DOM element 
	 *  
	 *  params:
	 *  object - data obtained from json
	 *  fromList - the list name which contains the current object. This will give the type of the current element being created.
	 *			 - areas: [area1, area2, area3] - processObject(area1, 'areas');
	 */
	AreaBuilder.prototype.process = function(object, fromList) {
		//domEl is a generic element which will contain the DOM el for object param. 
		var domEl;
		
		//deduce the object type and construct specific elements for current level.
		switch (fromList) {
			case AreaBuilder.List.ITEMS:
				return this.createItem(object);
			case AreaBuilder.List.AREAS:
				domEl = this.createArea(object.name);
				break;
			case AreaBuilder.List.CATEGORIES:
				domEl = this.createCategory(object.name);
				break;
			case AreaBuilder.List.SPECIALIZATIONS:
				domEl = this.createSpecialization();
		}
		
		//any field in object
		for (var field in object) {
			//which has the Array type
			if(object[field] instanceof Array) {
				//is a list, so iterate through it and construct subsequent elements
				var iterator = data.iteratorFor(object[field]);
				var current;
				
				//while there are items in the list
				while((current = iterator.currentItem())) {
					//process item and append to current domEl
					domEl.appendChild(this.process(current, field));
					
					//iterate
					current = iterator.next();
				}
			}
		}
		
		return domEl;
	};

	AreaBuilder.prototype.createSpecialization = function() {
		//create header title
		var header = new DomElement('h3').appendTextNode('Va rugam analizati "Ghidul de interviu tehnic" inainte de completare');
		
		//create datalist that will be used as suggestion for rating inputs
		var ratingList = new DomElement('datalist', { id : 'calificative' });
		for(var i = 1; i <= 10; i++) {
			ratingList.appendElement(new DomElement('option', { value : i }));
		}
		
		//create new section, append the header and ratingList and then return
		return new DomElement('section', {id : 'areas', name : 'areas'}).appendElement(header).appendElement(ratingList).get();
	};

	AreaBuilder.prototype.createArea = function(name) {
		this.currentArea = name;
		
		var area = new DomElement('fieldset');
		var legend = new DomElement('legend').appendTextNode(name || AreaBuilder.Defaults.ARIA_NAME);
		area.appendElement(legend);
		
		return area.get();
	};

	AreaBuilder.prototype.createCategory = function(name) {
		this.currentCategory = name || '';
		
		var category = new DomElement('div');
		
		if(name) {
			var categoryName = new DomElement('p', { class : 'categ-title'}).appendTextNode(name);
			category.appendElement(categoryName);
		}
		
		return category.get();
	}

	AreaBuilder.prototype.createItem = function(name) {
		var id = this.specialization + this.currentArea + (this.currentCategory ? '|' + this.currentCategory + '|' : '|') + name;
		var item = new DomElement('div').appendElement(new DomElement('p').appendTextNode(name))
										.appendElement(new DomElement('input', {list: 'calificative', name: id} ));
		
		return item.get();
	};

	AreaBuilder.prototype.appendTextNode = function(text, container) {
		if(!text || !container) {
			console.log("[[WARN]]: Invalid call: AreaBuilder.appendTextNode(" + text + ', ' + container + ')');
			return null;
		}
		
		var textNode = document.createTextNode(text);
		container.appendChild(textNode);
	};


	/** This class contains some util methods for creating and manipulating DOM elements
	 * 
	 * 	params: 
	 *  type: type of element - div, p, input, fieldset etc
	 *  attributes: optional attributes that can be added in current element node
	 *  css: optional css class that can be added in current element class list
	 */
	function DomElement(type, attributes, css) {
		if(!type) {
			console.log("[[WARN]]: Invalid call: DomElement(" + type + ')');
		}
		this.element = document.createElement(type);
			
		for(var attribute in attributes) {
			this.element.setAttribute(attribute, attributes[attribute]);
		}
		
		if(css) {
			this.element.className += ' ' + css;
		}
	};

	DomElement.prototype.appendElement = function(el) {
		this.element.appendChild(el.get());
		
		return this;
	};
		
	DomElement.prototype.appendTextNode = function(text) {
		if(!text) {
			console.log("[[WARN]]: Invalid call: Dom.appendTextNode(" + text + ')');
			return null;
		}
		
		var textNode = document.createTextNode(text);
		this.element.appendChild(textNode);
		
		return this;
	};
		
	DomElement.prototype.get = function() {
		return this.element;
	};
	
	return {
		build: function(parent, callback, url) {
			var areaBuilder = new AreaBuilder(parent, callback, url);
		}
	}
});