describe('build areas', function() {
	describe('valid json', function() {
		it('section title exists', function(done) {
			builder.build(document.getElementById('areas'), function(result){
				console.log("areas ready");
				
				var title = $('#areas h3');
				
				if(title.length === 1) {
					done();
				}
			}, 'http://localhost:8080/data/web.json');
		});
	});
});