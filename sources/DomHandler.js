define(['sources/AreaBuilder'], function DomHandler(builder) {
	var dateField = document.getElementById('interview-date');
	var addButton = document.getElementById('add-supervisor');
	var removeButton = document.getElementById('remove-supervisor');
	var supervisorName = document.getElementById('supervisor-name');
	var supervisors = document.getElementById('supervisors');
	var supervisor = document.getElementById('supervisor');
	
	dateField.value = currentDate();

	function currentDate() {
		var dateFormat = '';
		var date = new Date();

		dateFormat += date.getFullYear() + '-';
		
		if(date.getMonth() < 10) {
			dateFormat += '0';
		}

		dateFormat += date.getMonth() + '-';
		dateFormat += date.getDate();

		return dateFormat;
	}

	supervisor.addEventListener('focusin', onfocusin);
	supervisor.addEventListener('focusout', onfocusout);

	function onfocusin(e) {
		//console.log("focus in:", addButton.parentNode, this, e.currentTarget);
		if(this === addButton.parentNode) {
			addButton.style.display = 'inline-block';
		}
		else {
			this.appendChild(removeButton);
			removeButton.style.display = 'inline-block';
		}
	}
	
	addButton.addEventListener('mousedown', add);
	removeButton.addEventListener('mousedown', remove)

	function onfocusout() {
		addButton.style.display = 'none';
		removeButton.style.display = 'none';
	}

	function add() {
		//create new supervisor
		var container = document.createElement('div');
		var input = document.createElement('input');
		input.setAttribute('type', 'text');
		input.setAttribute('name', 'supervisor-name');
		
		input.className += 'large-text-input';
		container.addEventListener('focusin', onfocusin);
		container.addEventListener('focusout', onfocusout);
	
		container.appendChild(input);
		container.appendChild(this);
		supervisors.appendChild(container);
	}
	
	function remove() {
		supervisors.removeChild(this.parentNode);
	}

	//build areas and handle specific events
	builder.build(document.getElementById('form-container'), function() {
		//AreaBuilder will generate a section having its id = 'areas'
		var areaSection = document.getElementById('areas');
		function areaKeyUp(event) {
			var isDigit = function (code) { return (code >= 48 && code <= 57); };
			var isBetween = function (number, min, max) { return min <= number && number <= max; }
			
			if(!isDigit(event.charCode) || !isBetween(event.target.value + (event.charCode - 48), 1, 10)) {
				event.preventDefault();
			}
			
		}
		areaSection.addEventListener('keypress', areaKeyUp);
		
		
		function onFormSubmit(event) {
			//check mandatory fields
			var result = validateMandatoryFields();
			if(result.status === 'fail') {
				event.preventDefault();
				showError(result.message);
			}
			
			function validateMandatoryFields() {
				var error = null;
				var candidate = document.getElementById('candidate-name');
				var interviewDate = document.getElementById('interview-date');
				var supervisors = document.querySelectorAll('input[name="supervisor-name"]');
				
				if(candidate.value.trim() === '') {
					return {
						status: 'fail',
						message: 'Camp inexistent: nume candidat'
					};
				}
				
				if(interviewDate.value.trim() === '') {
					return {
						status: 'fail',
						message: 'Camp inexistent: data interviu'
					};
				}
				
				
				for(var i = 0; i < supervisors.length; i++) {
					if(supervisors[i].value.trim() === '') {
						return {
							status: 'fail',
							message: 'Camp inexistent: intervievator tehnic'
						}
					}
				}
				
				var levels = document.querySelectorAll('input[name="level"]');
				for(i = 0; i < levels.length; i++) {
					if(levels[i].checked)
						break;
				}
				
				if(i === levels.length) {
					return {
						status: 'fail',
						message: 'Nu a fost selectat un nivel tehnic'
					}
				}
				
				return {
					status: 'succes'
				}
			}
			
			function isEmpty(field) { return field.value.trim() === ''; }
			
			
			function showError(message) {
				var errorField = document.getElementById('form-error');
				var errorText = document.getElementById('error-text');
				var closeButton = document.getElementsByClassName('close')[0];
				
				errorField.style.display = 'block';
				errorText.innerHTML = message;
				
				closeButton.onclick = function() {
					errorField.style.display = 'none';
				};
				
				window.onclick = function(event) {
					console.log('window.onclick.target =', event.target);
					if (event.target === errorField) {
						errorField.style.display = "none";
						window.onclick = null;
					}
				}

			}

		}
		
		var form = document.getElementById('form-container');
		form.addEventListener('submit', onFormSubmit);
	});
});